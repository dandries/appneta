import React, { Component } from 'react';
import axios from 'axios';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import PlayerTable from './components/PlayerTable.jsx'
import './App.css';
import config from './config.js';

class App extends Component {
  
  constructor(props) {
    super(props)
    axios.defaults.baseURL = config.API_URL;
  }
  
  render() {
    return (
      <div>
        <AppBar position='static'>
          <Toolbar>
            <Typography variant='h6' color='inherit'>
              AppNeta
            </Typography>
          </Toolbar>
        </AppBar>
        <div className="App">
          
          <PlayerTable />
        </div>
      </div>
    );
  }
}

export default App;
