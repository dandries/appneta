import React from 'react'
import axios from 'axios'
import fecha from 'fecha'
import withStyles from '@material-ui/core/styles/withStyles'
import CircularProgress from '@material-ui/core/CircularProgress'

import PlayerFilter from './PlayerFilter'
import SortableDataTable from './SortableDataTable'
import Nationalities from './Nationalities'

const styles = {
  
}

const playerFields = [
  {
    name: 'id',
    label: 'ID'
  },
  {
    name: 'name',
    label: 'Name'
  },
  {
    name: 'nat',
    label: 'Nationality'
  },
  {
    name: 'pos',
    label: 'Position'
  },
  {
    name: 'height',
    label: 'Height (m)'
  },
  {
    name: 'weight',
    label: 'Weight (kg)'
  },
  {
    name: 'dob',
    label: 'DOB',
    contentAccess: (row) => fecha.format(new Date(row.dob), 'MMM D, YYYY')
  },
  {
    name: 'birthplace',
    label: 'Birth place'
  }
]

class PlayerTable extends React.Component {
  state = {
    players: null,
    filterValue: ''
  }

  componentDidMount() {
    axios.get('/players').then(({ data }) => {
      this.setState({ players: data })
    })
  }

  filteredData = () => {
    const { filterValue } = this.state
    if (!filterValue) {
      return this.state.players
    }
    const filterValueArray = filterValue.toLowerCase().split('')

    return this.state.players.filter((player) => {
      for (const field of playerFields) {
        if (typeof player[field.name] === 'string') {
          let isNotMatch = false
          for (const chr of filterValueArray) {
            if (player[field.name].toLowerCase().indexOf(chr) === -1) {
              isNotMatch = true
              break
            }
          }
          if (!isNotMatch) {
            return true
          }
        }
      }
      return false
    })
  }

  handleFilterChange = (ev) => {
    this.setState({ filterValue: ev.target.value })
  }

  table = (players) => {
    if (!players) {
      return (
        <CircularProgress />
      )
    }
    return (
      <SortableDataTable
        data={players}
        fields={playerFields}
      />
    )
  }

  render() {
    const players = this.state.players ? this.filteredData() : null
    return (
      <React.Fragment>
        <PlayerFilter
          onChange={this.handleFilterChange}
          value={this.state.filterValue}
        />
        <Nationalities
          players={players}
        />
        {this.table(players)}
      </React.Fragment>
    )
  }

}

export default withStyles(styles)(PlayerTable)