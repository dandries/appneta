import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Paper from '@material-ui/core/Paper'
import Chip from '@material-ui/core/Chip'

const styles = {
  paper: {
    padding: 16,
    marginBottom: 16,
    display: 'flex',
    flexWrap: 'wrap'
  }
}

const Nationalities = ({ classes, players }) => {
  if (!players) {
    return null
  }
  const nationalities = {

  }
  for (const player of players) {
    if (!nationalities[player.nat]) {
      nationalities[player.nat] = 0
    }
    nationalities[player.nat] += 1
  }
  return (
    <Paper classes={{ root: classes.paper }}>
      {Object.entries(nationalities).map((nationality, key) => (
        <Chip
          key={key}
          label={`${nationality[0]} (${nationality[1]})`}
        />
      ))}
    </Paper>
  )
}

export default withStyles(styles)(Nationalities)