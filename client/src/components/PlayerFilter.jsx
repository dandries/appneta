import React from 'react'
import withStyles from "@material-ui/core/styles/withStyles"
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'

const styles = {
  paper: {
    padding: 16,
    marginBottom: 16
  }
}

const PlayerFilter = ({ classes, value, onChange }) => (
  <Paper classes={{ root: classes.paper }}>
    <TextField
      onChange={onChange}
      value={value}
      fullWidth
      placeholder='Search...'
    />
  </Paper>
)

export default withStyles(styles)(PlayerFilter)