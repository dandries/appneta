import React from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Icon from '@material-ui/core/Icon'
import Paper from '@material-ui/core/Paper'

const styles = {
  tableHead: {
    '& th': {
      cursor: 'pointer',
      '& > div': {
        display: 'flex',
        alignItems: 'center'
      },
      '&:hover': {
        textDecoration: 'underline'
      }
    }
  },
  paper: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    overflowX: 'auto',
  }
}

class SortableDataTable extends React.Component {
  state = {
    sortField: null
  }

  handleSortSelect = (field) => {
    const { sortField } = this.state
    const newSortField = {
      field,
      isDesc: false
    }
    if (sortField && sortField.field.name === field.name) {
      newSortField.isDesc = !sortField.isDesc
    }
    this.setState({ sortField: newSortField })
  }

  sortIndicator = (field) => {
    if (!this.state.sortField || this.state.sortField.field.name !== field.name) {
      return null
    }
    let icon = 'arrow_drop_down'
    if (!this.state.sortField.isDesc) {
      icon = 'arrow_drop_up'
    }
    return (
      <Icon>
        {icon}
      </Icon>
    )
  }

  tableHead = () => {
    const { classes, fields } = this.props
    return (
      <TableHead classes={{ root: classes.tableHead }}>
        <TableRow>
          {fields.map((field, key) => (
            <TableCell
              onClick={() => this.handleSortSelect(field)}
              key={key}
            >
              <div>
                {field.label}
                {this.sortIndicator(field)}
              </div>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    )
  }

  tableContent = () => {
    const { data, fields } = this.props
    const { sortField } = this.state

    const sortedData = data.slice()
    if (sortField) {
      sortedData.sort((a, b) => {
        return a[sortField.field.name] < b[sortField.field.name]
      })
      if (sortField.isDesc) {
        sortedData.reverse()
      }
    }

    return (
      <TableBody>
        {sortedData.map((row, rowKey) => (
          <TableRow key={rowKey}>
            {fields.map((field, fieldKey) => (
              <TableCell key={fieldKey}>
                {field.contentAccess ?
                  field.contentAccess(row) : row[field.name]}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    )
  }

  render() {
    const { classes } = this.props
    return (
      <Paper className={classes.paper}>
        <Table>
          {this.tableHead()}
          {this.tableContent()}
        </Table>
      </Paper>
    )
  }
}

export default withStyles(styles)(SortableDataTable)

