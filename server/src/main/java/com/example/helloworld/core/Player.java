package com.example.helloworld.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Date;
import java.lang.Integer;
import java.lang.Float;

@Entity
@Table(name = "players")
@NamedQueries(
    {
        @NamedQuery(
            name = "com.example.helloworld.core.Player.findAll",
            query = "SELECT p FROM Player p"
        )
    })
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "number")
    private long number;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "nat", nullable = false)
    private String nat;

    @Column(name = "pos", nullable = false)
    private String pos;

    @Column(name = "height", nullable = false)
    private Float height;

    @Column(name = "weight", nullable = false)
    private Integer weight;

    @Column(name = "dob", nullable = false)
    private Date dob;

    @Column(name = "birthplace", nullable = false)
    private String birthplace;

    public Player() {
    }

    public Player(long number, String name, String nat, String pos, Float height,
        Integer weight, Date dob, String birthplace) {
        this.number = number;
        this.name = name;
        this.nat = nat;
        this.pos = pos;
        this.height = height;
        this.weight = weight;
        this.birthplace = birthplace;
        this.dob = dob;
    }

    public long getId() {
        return number;
    }

    public void setId(long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getNat() {
        return nat;
    }

    public void setNat(String nat) {
        this.nat = nat;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Player)) {
            return false;
        }

        final Player that = (Player) o;

        return Objects.equals(this.number, that.number) &&
                Objects.equals(this.name, that.name) &&
                Objects.equals(this.pos, that.pos) &&
                Objects.equals(this.dob, that.dob) &&
                Objects.equals(this.birthplace, that.birthplace) &&
                Objects.equals(this.weight, that.weight) &&
                Objects.equals(this.height, that.height) &&
                Objects.equals(this.nat, that.nat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name, pos, dob, birthplace,
            weight, height, nat);
    }
}
